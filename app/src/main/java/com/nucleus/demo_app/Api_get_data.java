package com.nucleus.demo_app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api_get_data {

    @GET("/api/users")
    public  Call<Response_data> get_data(@Query("page") int a);
}
