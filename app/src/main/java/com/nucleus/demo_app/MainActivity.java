package com.nucleus.demo_app;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Recycler_view_adapter recycler_view_adapter;
    ImageView loading_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loading_img=(ImageView)findViewById(R.id.img_loading);
       recyclerView=findViewById(R.id.recycler_view);
       layoutManager=new GridLayoutManager(this,2);
       recyclerView.setLayoutManager(layoutManager);
       get_data();






    }




    void get_data(){
        turn_on_loading();
        Call<Response_data> call=RetroClient
                .getInstance()
                .api_get_data()
                .get_data(1);

        call.enqueue(new Callback<Response_data>() {
            @Override
            public void onResponse(Call<Response_data> call, Response<Response_data> response) {
                //Toast.makeText(getApplicationContext(),"Data found",Toast.LENGTH_SHORT).show();

                String[] name=new String[response.body().getData().length];
                String[] email=new String[response.body().getData().length];
                String[] avaters=new String[response.body().getData().length];

                for (int i=0;i<response.body().getData().length;i++){
                 //   Log.d("email",response.body().getData()[i].getEmail());
                    name[i]=response.body().getData()[i].getFirst_name()+" "+response.body().getData()[i].getLast_name();
                    email[i]=response.body().getData()[i].getEmail();
                    avaters[i]=response.body().getData()[i].getAvatar();
                }

                set_data_recyler(name,email,avaters);
//                for (int i=0;i<name.length;i++){
//                    Log.d("name",name[i]);
//                }
            }

            @Override
            public void onFailure(Call<Response_data> call, Throwable t) {
                Log.e("error", t.toString());
                get_data();

            }
        });
    }

    private void set_data_recyler(String[] names ,String[] email,String[] avaters) {
        recycler_view_adapter=new Recycler_view_adapter(names,email,avaters,this);
        recyclerView.setAdapter(recycler_view_adapter);
        recyclerView.setHasFixedSize(true);
        turn_off_loading();

    }
    private void turn_on_loading() {
        loading_img.setVisibility(View.VISIBLE);
        Glide.with(this).asGif().load(R.drawable.loading).into(loading_img);

    }
    private  void turn_off_loading(){
        loading_img.setVisibility(View.GONE);
    }


}



