package com.nucleus.demo_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class Full_image extends AppCompatActivity {
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        imageView=(ImageView)findViewById(R.id.full_image);


        final String image_url=getIntent().getExtras().getString("image_url");
        //Toast.makeText(getApplicationContext(), image_url, Toast.LENGTH_SHORT).show();

        Glide.with(getApplicationContext())
                .load(image_url)
                .centerCrop()
                .into(imageView);

    }
}
