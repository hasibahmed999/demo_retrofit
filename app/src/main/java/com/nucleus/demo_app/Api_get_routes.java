package com.nucleus.demo_app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Hasib on 05/02/2019.
 */

public interface Api_get_routes {


    @POST("routes.php")
    Call<List<Response_get_routes>> getroutes();
}
