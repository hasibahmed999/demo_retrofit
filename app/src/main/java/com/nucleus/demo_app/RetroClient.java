package com.nucleus.demo_app;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {

    private static final  String base_url="https://reqres.in/";
    //private static final  String base_url="https://app.inyek.com/app_api/app_inyek/";
    private static Retrofit retrofit;
    private static  RetroClient mInstance;

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.SECONDS)
            .build();
    private RetroClient(){

        retrofit=new Retrofit.Builder()
                .baseUrl(base_url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static synchronized RetroClient getInstance(){

        if(mInstance==null) {
            mInstance = new RetroClient();
        }
        return mInstance;
    }


    public Api_get_data api_get_data(){
        return retrofit.create(Api_get_data.class);
    }

    public Api_get_routes getApi_getRoutes(){
        return  retrofit.create(Api_get_routes.class);
    }

}
