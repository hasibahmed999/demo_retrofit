package com.nucleus.demo_app;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Recycler_view_adapter extends RecyclerView.Adapter<Recycler_view_adapter.MyViewHolder> {

    String[] names;
    String[] email;
    String[] avaters;
    private  Context context;


    public Recycler_view_adapter(String[] names, String[] email,String[] avater,Context context) {
        this.names = names;
        this.email = email;
        this.avaters=avater;
        this.context=context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_item,viewGroup,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        viewHolder.textView.setText(names[i]);
        viewHolder.email.setText(email[i]);

        Glide.with(context)
                .load(avaters[i])
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.user_image);


        viewHolder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,Full_image.class);
                intent.putExtra("image_url",avaters[i]);
                intent.putExtra("name",names[i]);
                intent.putExtra("email",names[i]);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {

        return names.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView,email;
        ImageView user_image;
        LinearLayout item_layout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            user_image=itemView.findViewById(R.id.user_image);
            item_layout=itemView.findViewById(R.id.item_layout);

        }
    }


}
