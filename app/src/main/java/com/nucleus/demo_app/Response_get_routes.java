package com.nucleus.demo_app;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hasib on 05/02/2019.
 */

public class Response_get_routes {
    @SerializedName("response")
    private String response;

    @SerializedName("source")
    private  String source;

    @SerializedName("destination")
    private String destination;

    @SerializedName("id")
    private int id;

    public String getResponse() {
        return response;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public int getId() {
        return id;
    }
}
